package de.freewarepoint.cr;

/*
 * @author Jan Linden
*/
public class EvalField {

	private final int width, height;
	private final int[][] fieldEvalues;

	public EvalField(int width, int height) {
		this.width = width;
		this.height = height;
		fieldEvalues = new int[width][height];
	}

	public EvalField getCopy() {
		EvalField copy = new EvalField(getWidth(), getHeight());
		for (int x = 0; x < getWidth(); ++x) {
			for (int y = 0; y < getHeight(); ++y) {
				int originalValue = getValueAt(x, y);
				copy.setValueAt(x, y, originalValue);
			}
		}
		return copy;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public int getValueAt(int x, int y) {
		return fieldEvalues[x][y];
	}
	
	public int getValueAt(CellCoordinateTuple cellCoordinate) {
		return fieldEvalues[cellCoordinate.x][cellCoordinate.y];
	}
	
	public void setValueAt(int x, int y, int value) {
		fieldEvalues[x][y] = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(int[] row:fieldEvalues){
			for(int val: row){
				builder.append(String.valueOf(val) + ", ");
			}builder.append("\n");
		}
		return builder.toString();
	}
}
