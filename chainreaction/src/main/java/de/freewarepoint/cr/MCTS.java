package de.freewarepoint.cr;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/*
 * @author Jan Linden
*/
public class MCTS {
    private static final double EPS = 1e-8;

    public class Edge {
        String s;
        int[] a;

        Edge(String f, int d, int e) {
            s = f;
            a = new int[]{d, e};
        }

        Edge(String f, int[] r) {
            assert r.length == 2;
            s = f;
            a = r;
        }

        @Override
        public String toString() {
            return s +" "+ a[0] + a[1];
        }
    }

    Strategy strategy;

    Map<String, Double> Qsa;
    Map<String, Integer> Nsa;
    Map<String, Integer> Ns;
    Map<String, Double[]> Ps;

    Map<String, Integer> Es;
    Map<String, Boolean[]> Vs;

    int numMTCSSims;
    double cpuct;


    public interface Strategy {
        Double[] policy(Field field);

        Double value(Double[] policy);
    }

    public MCTS(Strategy strategy, int numMTCSSims, double cpuct) {
        this.strategy = strategy;

        this.numMTCSSims = numMTCSSims;
        this.cpuct = cpuct;

        this.Qsa = new HashMap<>();
        this.Nsa = new HashMap<>();
        this.Ns = new HashMap<>();
        this.Ps = new HashMap<>();

        this.Es = new HashMap<>();
        this.Vs = new HashMap<>();
    }

    public Double[] getActionProb(Field field) {
        return getActionProb(field, 1);
    }

    public Double[] getActionProb(Field field, double temp) {
        for (int i = 0; i < numMTCSSims; i++)
            search(field);

        String s = field.toString();
        int actionSize = field.getWidth() * field.getHeight();
        Double[] probs = new Double[actionSize];
        Integer[] counts = new Integer[actionSize];
        for (int i = 0; i < actionSize; i++) {
            probs[i]=0.0;
            String key = new Edge(s, actionToTuple(i)).toString();
            if (Nsa.containsKey(key)) {
                counts[i] = Nsa.get(key);
            } else {
                counts[i] = 0;
            }
        }

        if (temp == 0) {
            int best = Collections.max(Arrays.asList(counts));
            int bestA = 0;
            for (int i = 0; i < actionSize; i++) {
                if(counts[i]==best){
                    bestA=i;
                    break;
                }
            }

            probs[bestA] = 1.0;
            return probs;
        }

        Double[] ncounts = new Double[actionSize];
        double sum = 0;
        for (int i = 0; i < actionSize; i++) {
            ncounts[i] = Math.pow(1.0 * counts[i], 1.0 / temp);
            sum += ncounts[i];
        }
        for (int i = 0; i < actionSize; i++) {
            probs[i] = ncounts[i] / sum;
        }
        return probs;

    }

    public double search(Field field) {
        String s = field.toString();
        int actionSize = field.getHeight() * field.getWidth();
        if (!Es.containsKey(s)) {
            Es.put(s, field.getGameEnded());
        }
        if (Es.get(s) != 0) {
            /*terminal node*/
            return -Es.get(s);
        }
        if (!Ps.containsKey(s)) {
            /*unvisited (leaf) node*/
            Ps.put(s, strategy.policy(field));
            Double[] old = strategy.policy(field);
            Double[] policy = Ps.get(s);
            Boolean[] valids = new Boolean[actionSize];
            for (int i = 0; i < actionSize; i++) {
                int[] t = actionToTuple(i);
                valids[i] = field.getOwnerOfCellAtPosition(t[0], t[1]) != Player.SECOND;
                //policy[i] += 1;
            }
            /*masking invalid*/
            double sum = 0;
            for (int i = 0; i < actionSize; i++) {
                if (!valids[i]) {
                    policy[i] = 0.0;
                }
                sum += policy[i];
            }
            /*
            System.out.println(field);
            for (int i = 0; i < actionSize; i++) System.out.print(old[i]+" ");
            System.out.println();
            for (int i = 0; i < actionSize; i++) System.out.print(valids[i]+" ");
            System.out.println();
            for (int i = 0; i < actionSize; i++) System.out.print(policy[i]+" ");
            System.out.println();
            System.out.println(sum);
            */
            if (sum > 0) {
                for (int i = 0; i < actionSize; i++) {
                    policy[i] /= sum;
                }
            } else {
                System.out.println("All valid moves were masked, do workaround.");
                double newsum = 0;
                for (int i = 0; i < actionSize; i++) {
                    policy[i] = (valids[i]) ? 1.0 : 0.0;
                    newsum += policy[i];
                }
                for (int i = 0; i < actionSize; i++) {
                    policy[i] /= newsum;
                }
            }
            assert Ps.get(s) == policy;
            Vs.put(s, valids);
            Ns.put(s, 0);
            return -strategy.value(strategy.policy(field));
        }
        Boolean[] valids = Vs.get(s);
        double cur_best = -Double.MAX_VALUE;
        int best_act = -1;
        /*maximise upper confidence bound*/
        for (int i = 0; i < actionSize; i++) {
            if (valids[i]) {
                double u;
                String edge = new Edge(s, actionToTuple(i)).toString();
                if (Qsa.containsKey(edge)) {
                    u = Qsa.get(edge) + cpuct * Ps.get(s)[i] * Math.sqrt(Ns.get(s)) / (1 + Nsa.get(edge));
                } else {
                    u = cpuct * Ps.get(s)[i] * Math.sqrt(Ns.get(s) + EPS);
                }
                if (u > cur_best) {
                    cur_best = u;
                    best_act = i;
                }
            }
        }
        int a = best_act;
        String edge = new Edge(s, actionToTuple(a)).toString();
        int[] action = actionToTuple(a);
        //System.out.println("start");
        //System.out.println(field);
        field = field.getCopy();
        field.putAtom(Player.FIRST, action[0], action[1]);
        //Field f2 = field.getCopy();
        field.react();
        /*System.out.println(field);
        if(!field.isStable() && field.getGameEnded() == 0){
            f2.react();
        }*/
        field = field.getCanonicalForm(Player.SECOND);
        //System.out.println(field);

        double v = search(field);

        if (Qsa.containsKey(edge)) {
            Qsa.put(edge, (Nsa.get(edge) * Qsa.get(edge) + v) / (Nsa.get(edge) + 1.0));
            Nsa.put(edge, Nsa.get(edge) + 1);
        } else {
            Qsa.put(edge, v);
            Nsa.put(edge, 1);
        }
        Ns.put(s, Ns.get(s) + 1);
        return -v;
    }

    public static int[] actionToTuple(int a) {
        return new int[]{a % 6, a / 6};
    }
}
