package de.freewarepoint.cr.ai;

import de.freewarepoint.cr.Field;
import de.freewarepoint.cr.Game;
import de.freewarepoint.cr.Player;
import de.freewarepoint.cr.UtilMethods;

import java.util.Random;

/*
 * @author Jan Linden
*/
public class FiveHeadedHydra implements AI {

    private Game game;

    public void doMove() {
        Field field = game.getField();
        Player playerAI = game.getCurrentPlayer();
        Player playerOpposing = playerAI == Player.SECOND ? Player.FIRST : Player.SECOND;

        int[] coords = ThreeHeadedHydra.minmax(game, 5, playerAI, playerOpposing);
        game.selectMove(coords[0], coords[1]);

    }

    @Override
    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public String getName() {
        return "FiveHeadedHydra";
    }
}
