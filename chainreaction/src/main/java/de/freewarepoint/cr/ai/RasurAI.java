package de.freewarepoint.cr.ai;

import de.freewarepoint.cr.*;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 * @author Jan Linden
 */
public class RasurAI implements AI{
    int[][] eval;
    MCTS.Strategy strategy = new MCTS.Strategy() {
        StandardAI standardAI = new StandardAI();
        @Override
        public Double[] policy(Field field) {
            Double[] policy = new Double[30];
            int opposingAtoms = UtilMethods.countOwnedAtoms(field, Player.SECOND);
            for(int x = 0; x < field.getWidth(); ++x) {
                for(int y = 0; y < field.getHeight(); ++y) {
                    policy[x+y*field.getWidth()]=1.0*standardAI.calculateCellValue(field, x, y, Player.FIRST, Player.SECOND, opposingAtoms);
                }
            }
            return policy;
        }

        @Override
        public Double value(Double[] policy) {
            return Collections.max(Arrays.asList(policy))/150;
        }
    };
    private Game game;
    private int[] think2(Field f,Player playerAI,Player playerOpposing) {
        int actionSize = f.getWidth()*f.getHeight();
        MCTS mcts = new MCTS(strategy,120,1);
        Double[] prob = mcts.getActionProb(f.getCanonicalForm(playerAI),0.8);

        int besta = -1;
        double curbest = 0;
        Random r = new Random();
        for(int i=0;i<actionSize;i++){
            eval[i%game.getField().getWidth()][i/game.getField().getWidth()] = (int) Math.floor(prob[i]*100);
            if(prob[i]>curbest|| (r.nextBoolean() && prob[i]>=curbest)){
                besta = i;
                curbest=prob[i];
            }
        }
        /*System.out.println(playerAI);
        System.out.println(f.toString());
        System.out.println(besta);
        int[] a = new StandardAI().think(f,playerAI,playerOpposing);
        System.out.println(a[0]+6*a[1]);
        */
        return MCTS.actionToTuple(besta);

    }
    public void doMove(){
        Field field = game.getField();
        Player playerAI = game.getCurrentPlayer();
        Player playerOpposing = playerAI == Player.SECOND ? Player.FIRST : Player.SECOND;

        int[] coords = think2(field, playerAI, playerOpposing);
        game.setCurrentEvalField(eval);
        game.selectMove(coords[0], coords[1]);
    }

    @Override
    public void setGame(Game game) {
        this.game = game;
        eval = new int[game.getField().getWidth()][game.getField().getHeight()];
    }

    @Override
    public String getName() {
        return "Rasur AI";
    }
}
