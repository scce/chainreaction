package de.freewarepoint.cr.ai;

import de.freewarepoint.cr.Field;
import de.freewarepoint.cr.Game;
import de.freewarepoint.cr.Player;
import de.freewarepoint.cr.UtilMethods;

import java.util.Random;

/**
 * @author Jan Linden
 */
public class ThreeHeadedHydra implements AI {

    private Game game;
    private static UtilMethods util = new UtilMethods();

    static class Tuple {
        int score;
        int[] place;

        Tuple(int s, int[] p) {
            score = s;
            place = p;
        }
    }

    public static int[] minmax(Game g, int depth, Player current, Player enemy) {
        return minmax(g.getField(), depth, current, enemy, -1, Integer.MAX_VALUE, true).place;
    }

    private static Tuple minmax(Field f, int depth, Player current, Player enemy, int alpha, int beta, boolean max) {
        int[] coords = new int[2];
        int score;
        if (depth == 1) {
            Random r = new Random();
            score = Integer.MIN_VALUE;
            for (int x = 0; x < f.getWidth(); ++x) {
                for (int y = 0; y < f.getHeight(); ++y) {
                    int cellvalue = calculateCellValue(f, x, y, current, enemy);
                    if (cellvalue > score || (r.nextBoolean() && cellvalue >= score)) {
                        score = cellvalue;
                        coords[0] = x;
                        coords[1] = y;
                    }
                }
            }
            return new Tuple(score, coords);
        }
        if (max) {
            score = -1;
            for (int x = 0; x < f.getWidth(); ++x) {
                for (int y = 0; y < f.getHeight(); ++y) {
                    if (f.getOwnerOfCellAtPosition(x, y) == enemy) continue;
                    Field copyf = f.getCopy();
                    copyf.putAtom(current, x, y);
                    copyf.react();
                    if(UtilMethods.countOwnedAtoms(copyf,enemy)==0 && UtilMethods.countOwnedAtoms(copyf,current)>1){
                        coords[0] = x;
                        coords[1] = y;
                        return new Tuple(Integer.MAX_VALUE, coords);
                    }
                    Tuple res = minmax(copyf, depth - 1, enemy, current, alpha, beta, false);
                    int newscore = Math.max(res.score,0);
                    alpha = Math.max(alpha,score);
                    if (newscore > score) {
                        score = newscore;
                        coords[0] = x;
                        coords[1] = y;
                    }
		    if(alpha >= beta){
                        return new Tuple(score, coords);
                    }
                }
            }
        } else {
            score = Integer.MAX_VALUE;
            for (int x = 0; x < f.getWidth(); ++x) {
                for (int y = 0; y < f.getHeight(); ++y) {
                    if (f.getOwnerOfCellAtPosition(x, y) == enemy) continue;
                    Field copyf = f.getCopy();
                    copyf.putAtom(current, x, y);
                    copyf.react();
                    if(UtilMethods.countOwnedAtoms(copyf,enemy)==0 && UtilMethods.countOwnedAtoms(copyf,current)>1){
                        coords[0] = x;
                        coords[1] = y;
                        return new Tuple(Integer.MIN_VALUE, coords);
                    }
                    Tuple res = minmax(copyf, depth - 1, enemy, current, alpha, beta, true);
                    int newscore = Math.max(res.score,0);
                    beta = Math.min(beta,score);
                    if (newscore < score) {
                        score = newscore;
                        coords[0] = x;
                        coords[1] = y;
                    }
		    if(alpha >= beta){
                        return new Tuple(score, coords);
                    }
                }
            }
        }
        return new Tuple(score, coords);
    }

    public static int calculateCellValue(Field f, int x, int y, Player playerAI, Player playerOpposing) {

        Player owner = f.getOwnerOfCellAtPosition(x, y);
        int opposingAtoms = util.countOwnedAtoms(f, playerOpposing);
        if (owner == Player.NONE || owner == playerAI) {
            Field fieldAI = util.getCopyOfField(f);
            util.placeAtom(fieldAI, x, y, playerAI);
            util.reactField(fieldAI);
            if (util.countOwnedAtoms(fieldAI, playerOpposing) == 0) return 9001;
            int tmp = util.countPlayerCells(fieldAI, playerAI);
            tmp += util.countOwnedAtoms(fieldAI, playerAI);
            tmp += opposingAtoms - util.countOwnedAtoms(fieldAI, playerOpposing);
            tmp += util.isCornerCell(fieldAI, x, y) ? 1 : 0;
            tmp += util.countCriticalFieldsForPlayer(fieldAI, playerAI) * 2;
            tmp -= util.computeDangerForCell(fieldAI, x, y, playerAI) * 4;
            tmp -= util.countEndangeredFields(fieldAI, playerAI);
            return (tmp < 0 ? 0 : tmp);
        }
        return -1;
    }

    public void doMove() {
        Player playerAI = game.getCurrentPlayer();
        Player playerOpposing = playerAI == Player.SECOND ? Player.FIRST : Player.SECOND;

        int[] coords = minmax(game, 3, playerAI, playerOpposing);
        game.selectMove(coords[0], coords[1]);

    }

    @Override
    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public String getName() {
        return "ThreeHeadedHydra";
    }
}
