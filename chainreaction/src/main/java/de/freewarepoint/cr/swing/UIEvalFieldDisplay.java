package de.freewarepoint.cr.swing;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.*;

import de.freewarepoint.cr.EvalField;
import de.freewarepoint.retrofont.RetroFont;

/**
 * @author Dennis Kuehn
 */
public class UIEvalFieldDisplay extends JPanel {
	
	private static final long serialVersionUID = -5501029406858034003L;
	final RetroFont retroFont = new RetroFont();
	private EvalField evalField;

	private JCheckBox autoplay;
	private boolean autoplayEnabled;

	private JButton next;
	private JPanel numbers;

	private UIGame uiGame;

	public UIEvalFieldDisplay(EvalField evalField, final UIGame uiGame) {
		super();
		this.uiGame = uiGame;
		autoplayEnabled = true;
		
		setDoubleBuffered(true);
		numbers = new JPanel();
		
		numbers.setLayout(new GridLayout(evalField.getHeight(), evalField.getWidth(), 15, 15));
		this.setLayout(new FlowLayout());

		autoplay = new JCheckBox("Autoplay");
		autoplay.setSelected(true);
		autoplay.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED){
					autoplayEnabled = true;
				}else{
					autoplayEnabled = false;
				}
			}
		});

		next = new JButton("Next");
		next.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				synchronized (uiGame) {
					uiGame.notify();
				}
			}
		});
		setEvalField(evalField);
	}

	public boolean setEvalField(EvalField evalField){
		//if(this.evalField != null && this.evalField.equals(evalField)) return autoplayEnabled;
		this.removeAll();
		this.evalField = evalField;
		numbers = new JPanel();
		numbers.setLayout(new GridLayout(evalField.getHeight(), evalField.getWidth(), 15, 15));
		this.add(numbers);
		JPanel bts = new JPanel();
		bts.add(autoplay);
		bts.add(next);
		bts.setPreferredSize(new Dimension(100,100));
		this.add(bts);
		final JLabel[][] evalFieldLabels = new JLabel[evalField.getWidth()][evalField.getHeight()];
		for (int y = 0; y < evalFieldLabels[0].length; y++) {
			for (int x = 0; x < evalFieldLabels.length; x++) {
				final JLabel label = new JLabel();
				label.setIcon(new ImageIcon(retroFont.getRetroString(evalField.getValueAt(x, y) + "", Color.BLACK, 32)));
				numbers.add(label);
				evalFieldLabels[x][y] = label;
			}
		}
		numbers.revalidate();
		numbers.repaint();
		return autoplayEnabled;
	}

	@Override
	public String toString() {
		if(evalField == null) return "";
		return evalField.toString();
	}
}
